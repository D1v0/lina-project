﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Linq.Expressions;

namespace BaseModels
{
    public class BaseModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Метод генерации события изменения свойства объекта
        /// </summary>
        /// <param name="propertyName">Имя параметра</param>
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Метод генерации события изменения свойства объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="action">Лямбда функция доступа свойству</param>
        protected void OnPropertyChanged<T>(Expression<Func<T>> action)
        {
            var propertyName = GetPropertyName(action);
            OnPropertyChanged(propertyName);
        }

        /// <summary>
        /// Метод получения имени парметра
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="action">Лямбда функция доступа свойству</param>
        /// <returns>Возвращает имя параметра</returns>
        private static string GetPropertyName<T>(Expression<Func<T>> action)
        {
            var expression = (MemberExpression)action.Body;
            var propertyName = expression.Member.Name;
            return propertyName;
        }


        /// <summary>
        /// Событие изменения свойств объекта
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
