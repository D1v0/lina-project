﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseModels
{
    public class StorageStructConst : BaseModel
    {
        private uint _standartBlockSize;
        public uint standartBlockSize
        {
            get { return _standartBlockSize; }
            set { _standartBlockSize = value; OnPropertyChanged(() => standartBlockSize); }
        }

        
        private string _storageName;
        public string storageName
        {
            get { return _storageName; }
            set { _storageName = value; OnPropertyChanged(() => storageName); }
        }

        
        private long _storageSize;
        public long storageSize
        {
            get { return _storageSize; }
            set { _storageSize = value; OnPropertyChanged(() => storageSize); }
        }

        
        private bool _isFirstWrite;
        public bool isFirstWrite
        {
            get { return _isFirstWrite; }
            set { _isFirstWrite = value; OnPropertyChanged(() => isFirstWrite); }
        }
        
        
        
        private string _dateTime;
        public string dateTime
        {
            get { return _dateTime; }
            set { _dateTime = value; OnPropertyChanged(() => dateTime); }
        }

        
        private uint _blockCount;
        public uint blockCount
        {
            get { return _blockCount; }
            set { _blockCount = value; OnPropertyChanged(() => blockCount); }
        }

        
        private uint _fillBlocks;
        public uint fillBlocks
        {
            get { return _fillBlocks; }
            set { _fillBlocks = value; OnPropertyChanged(() => fillBlocks); }
        }

        
        private uint _freeBlocks;
        public uint freeBlocks
        {
            get { return _freeBlocks; }
            set { _freeBlocks = value; OnPropertyChanged(() => freeBlocks); }
        }

        
        private int[] _indexFreeBlocks;
        public int[] indexFreeBlocks
        {
            get { return _indexFreeBlocks; }
            set { _indexFreeBlocks = value; OnPropertyChanged(() => indexFreeBlocks); }
        }

        
        private uint[] _structSize;
        public uint[] structSize
        {
            get { return _structSize; }
            set { _structSize = value; OnPropertyChanged(() => structSize); }
        }

        
        private uint[] _SHA1Table;
        public uint[] SHA1Table
        {
            get { return _SHA1Table; }
            set { _SHA1Table = value; OnPropertyChanged(() => SHA1Table); }
        }
        
        private uint _serviceStructSize;
        public uint serviceStructSize
        {
            get { return _serviceStructSize; }
            set { _serviceStructSize = value; OnPropertyChanged(() => serviceStructSize); }
        }


        
        private byte[] _password;
        public byte[] password
        {
            get { return _password; }
            set { _password = value; OnPropertyChanged(() => password); }
        }

        
        private byte _encryptionAlg;
        public byte encryptionAlg
        {
            get { return _encryptionAlg; }
            set { _encryptionAlg = value; OnPropertyChanged(() => encryptionAlg); }
        }
        
        
        
    }
}
