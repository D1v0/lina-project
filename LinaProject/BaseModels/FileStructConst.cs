﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseModels
{
   public class FileStructConst : BaseModel
    {
        
        private uint _Nubmer;
        public uint Nubmer
        {
            get { return _Nubmer; }
            set { _Nubmer = value; OnPropertyChanged(() => Nubmer); }
        }

        
        private string _fileName;
        public string fileName
        {
            get { return _fileName; }
            set { _fileName = value; OnPropertyChanged(() => fileName); }
        }

        
        private uint _structSize;
        public uint structSize
        {
            get { return _structSize; }
            set { _structSize = value; OnPropertyChanged(() => structSize); }
        }

        
        private uint _deduplicatedBlockCount;
        public uint deduplicatedBlockCount
        {
            get { return _deduplicatedBlockCount; }
            set { _deduplicatedBlockCount = value; OnPropertyChanged(() => deduplicatedBlockCount); }
        }

        
        private uint _uniqueBlockCount;
        public uint uniqueBlockCount
        {
            get { return _uniqueBlockCount; }
            set { _uniqueBlockCount = value; OnPropertyChanged(() => uniqueBlockCount); }
        }


        
        private uint[] _usedBlocks;
        public uint[] usedBlocks
        {
            get { return _usedBlocks; }
            set { _usedBlocks = value; OnPropertyChanged(() => usedBlocks); }
        }
        

        
        private uint _valuedBytes;
        public uint valuedBytes
        {
            get { return _valuedBytes; }
            set { _valuedBytes = value; OnPropertyChanged(() => valuedBytes); }
        }
        
        
        private bool _onDelete;
        public bool onDelete
        {
            get { return _onDelete; }
            set { _onDelete = value; OnPropertyChanged(() => onDelete); }
        }

        /// <summary>
        /// Общий отступ текущей структуры, при всех существующих структурах. Параметр навигации. Не пишется в хранилище.
        /// </summary>
        private uint _structOffset;
        public uint structOffset
        {
            get { return _structOffset; }
            set { _structOffset = value; OnPropertyChanged(() => structOffset); }
        }

       /// <summary>
       /// Индекс использующийся при формировании массивов с дуплецированными или уникальными блоками.
       /// Параметр навигации. В хранилище не пишется.
       /// </summary>
 
        private uint _localIndex=0;
        public uint localIndex
        {
            get { return _localIndex; }
            set { _localIndex = value; OnPropertyChanged(() => localIndex); }
        }

        
        private int[] _SHA1Table;
        public int[] SHA1Table
        {
            get { return _SHA1Table; }
            set { _SHA1Table = value; OnPropertyChanged(() => SHA1Table); }
        }
        
        
        

    }
}
