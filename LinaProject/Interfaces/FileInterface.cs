﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;
using System.IO;

namespace Interfaces
{
    public interface FileInterface
    {
        string Path { get; set; }
        FileStream Stream { get; set; }
    }
}
