﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface CryptoInterface
    {
         byte[] Encrypt(byte[] Block);
         byte[] Decrypt(byte[] Block);
    }
}
