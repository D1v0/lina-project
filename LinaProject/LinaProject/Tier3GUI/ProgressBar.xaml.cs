﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using BaseModels;


namespace LinaProject.Tier3GUI
{
    /// <summary>
    /// Логика взаимодействия для ProgressBar.xaml
    /// </summary>
    public partial class ProgressBar : Window
    {
        public LittleModel model;
        public ProgressBar(int min, int max, int val)
        {
            model = new LittleModel();
            model.min = min;
            model.max = max;
            model.val = val;            
            InitializeComponent();
            this.DataContext = model;            
        }     

        
    }

    public class LittleModel : BaseModel
    {
        
        private int _min;
        public int min
        {
            get { return _min; }
            set { _min = value; OnPropertyChanged(() => min); }
        }

        
        private int _max;
        public int max
        {
            get { return _max; }
            set { _max = value; OnPropertyChanged(() => max); }
        }

        

       private int _val;
       public int val
        {
            get { return _val; }
            set {
                _val = value;
                OnPropertyChanged(() => val);
            }
        }
              
    }
}
