﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseModels;

namespace LinaProject.Tier3GUI
{
    public class StorageDataGrid : BaseModel
    {
        public string storageName { get; set; }
        public string dateTime { get; set; }
        public uint blockCount { get; set; }
        public uint blockSize { get; set; }
        public uint fillBlocks { get; set; }
        public uint freeBlocks { get; set; }
        public long storageSize { get; set; }
        public long freeSpace { get; set; }

        public string hstorageName = "Имя хранилища:";
        public string hdateTime = "Дата создания:";
        public string hblockCount = "Общее кол-во блоков:";
        public string hblockSize = "Размер блока(байт):";
        public string hfillBlocks = "Заполнено блоков:";
        public string hfreeBlocks = "Свободно блоков:";
        public string hstorageSize = "Размер хранилища(байт):";
        public string hfreeSpace = "Свободное пространство(байт):";
 
        public StorageDataGrid(string Name,string dateTime,uint blockCount,uint blockSize,uint fillBlocks, uint freeBlocks, long storageSize)
        {
            this.storageName = Name;
            this.dateTime = dateTime;
            this.blockCount = blockCount;
            this.blockSize = blockSize;
            this.fillBlocks = fillBlocks;
            this.freeBlocks = freeBlocks;
            this.storageSize = storageSize;
            this.freeSpace = ((long)this.freeBlocks * (long)this.blockSize);
        }

        public StorageDataGrid()
        {
            this.storageName = "Недоступно";
            this.dateTime = DateTime.Today.ToString();
            this.blockCount = 0;
            this.blockSize = 0;
            this.fillBlocks = 0;
            this.freeBlocks = 0;
            this.storageSize = 0;
            this.freeSpace = 0;
        }
    }

    public class StorageTableGrid : BaseModel
    {
        public string Header { get; set; }
        public string Data { get; set; }

        public StorageTableGrid(string Header, string Data)
        {
            this.Header = Header;
            this.Data = Data;
        }
    }
}
