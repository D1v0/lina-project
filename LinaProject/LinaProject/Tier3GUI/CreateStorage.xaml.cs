﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Interfaces;

namespace LinaProject.Tier3GUI
{
    /// <summary>
    /// Логика взаимодействия для CreateStorage.xaml
    /// </summary>
    public partial class CreateStorage : Window
    {
        ModelForCreate model;
        NewWindowModel localModel;
        public CreateStorage()
        {
            model = new ModelForCreate();
            InitializeComponent();
            this.DataContext = model;
            selectBlockMeasure.SelectedIndex = 0;
            selectMeasure.SelectedIndex = 0;
            cmbAlg.SelectedIndex = 0;
        }

        public CreateStorage(ref NewWindowModel localModel)
        {
            model = new ModelForCreate();
            InitializeComponent();
            this.DataContext = model;
            selectBlockMeasure.SelectedIndex = 1;
            selectMeasure.SelectedIndex = 0;
            cmbAlg.SelectedIndex = 0;
            this.localModel = localModel;
        }

       /* private void selectBlockMeasure_DropDownClosed(object sender, EventArgs e)
        {
            string arg = selectBlockMeasure.SelectionBoxItem.ToString();

            switch (arg)
            {
                case "Byte":
                    selectBlockSize.Minimum = 1024;
                    selectBlockSize.Maximum = 32768;
                    selectBlockSize.SmallChange = 1024;
                    selectBlockSize.TickFrequency = 1024;
                    model.blockSize *= 1024;
                    break;
                case "KByte":
                    selectBlockSize.Minimum = 1;
                    selectBlockSize.Maximum = 32;
                    selectBlockSize.SmallChange = 1;
                    selectBlockSize.TickFrequency = 1;
                    model.blockSize /= 1024;
                    break;
            }
        }
        */
        private void selectMeasure_DropDownClosed(object sender, EventArgs e)
        {
            string arg = selectMeasure.SelectionBoxItem.ToString();

            switch (arg)
            {
                case "MByte":
                    selectSizeStorage.Minimum = 1;
                    selectSizeStorage.Maximum = 5120;
                    selectSizeStorage.SmallChange = 1;
                    selectSizeStorage.TickFrequency = 10;
                    model.storageSize *= 1024;
                    break;
                case "GByte":
                    selectSizeStorage.Minimum = 1;
                    selectSizeStorage.Maximum = 5;
                    selectSizeStorage.SmallChange = 1;
                    selectSizeStorage.TickFrequency = 1;
                    model.storageSize /= 1024;
                    break;
            }
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Lina block storage unit"; // Default file name
            dlg.DefaultExt = ".lbsu"; // Default file extension
            dlg.Filter = "Lina block storage unit (.lbsu)|*.lbsu"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                localModel.create = true;
                localModel.PathToStorage = dlg.FileName;
                localModel.password = passwordBox.Password;
                localModel.blockSize = model.blockSize*1024;
                switch (cmbAlg.SelectedItem.ToString())
                {
                    case "Advanced Encryption Standart (AES)":
                        {
                            localModel.encryptionAlg = 1;
                            break;
                        }
                    case "Blowfish":
                        {
                            localModel.encryptionAlg = 2;
                            break;
                        }
                    case "Без шифрования":
                        {
                            localModel.encryptionAlg = 0;
                            break;
                        }
                }
                if (selectBlockMeasure.SelectionBoxItem == "Byte")
                {
                    localModel.blockSize = model.blockSize;
                }
                else
                {
                    localModel.blockSize = model.blockSize*1024;
                }
                if (selectMeasure.SelectionBoxItem == "MByte")
                {
                    localModel.storageSize = model.storageSize*1024*1024;
                }
                else
                {
                    localModel.storageSize = model.storageSize * 1024 * 1024*1024;
                }
            }
            else
            {
                localModel.create = false;
            }
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
