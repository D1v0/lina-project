﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LinaProject.Tier3GUI
{
    /// <summary>
    /// Логика взаимодействия для LogIn.xaml
    /// </summary>
    public partial class LogIn : Window
    {
        NewWindowModel localmodel;
        public LogIn()
        {
            InitializeComponent();
        }

        public LogIn(ref NewWindowModel localmodel)
        {
            InitializeComponent();
            this.localmodel = localmodel;
        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            localmodel.password = pswdBox.Password;
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
