﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinaProject.Tier3GUI
{
    class ModelForCreate : BaseModels.BaseModel
    {
        

        private string _path = "Укажите местоположение";
        public string path
        {
            get { return _path; }
            set { _path = value; OnPropertyChanged(() => path); }
        }

        
        private long _storageSize=10;
        public long storageSize
        {
            get { return _storageSize; }
            set { _storageSize = value; OnPropertyChanged(() => storageSize); }
        }

        
        private uint _blockSize=4;
        public uint blockSize
        {
            get { return _blockSize; }
            set { _blockSize = value; OnPropertyChanged(() => blockSize); }
        }

        
        private List<string> _measurementStorageSize = new List<string>(){"MByte","GByte"};
        public List<string> measurementStorageSize
        {
            get { return _measurementStorageSize; }
            set { _measurementStorageSize = value; OnPropertyChanged(() => measurementStorageSize); }
        }


        private List<string> _encryptionAlg = new List<string>(){ "Advanced Encryption Standart (AES)", "Blowfish", "Без шифрования" };
        public List<string> encryptionAlg
        {
            get { return _encryptionAlg; }
            set { _encryptionAlg = value; OnPropertyChanged(() => encryptionAlg); }
        }

        private List<string> _measurementBlock=new List<string>(){"KByte"};
        public List<string> measurementBlock
        {
            get { return _measurementBlock; }
            set { _measurementBlock = value; OnPropertyChanged(() => measurementBlock); }
        }



        
        private string _password;
        public string password
        {
            get { return _password; }
            set { _password = value; OnPropertyChanged(() => password); }
        }
        
        
    }
}
