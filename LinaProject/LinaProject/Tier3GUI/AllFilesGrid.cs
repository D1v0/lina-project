﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinaProject.Tier3GUI
{
    public class AllFilesGrid
    {
        public string sFile { get; set; }
        public AllFilesGrid(string fileName)
        {
            this.sFile = fileName;
        }

        public AllFilesGrid()
        {
            this.sFile = "Файлов нет";
        }

        public override string ToString()
        {
            return this.sFile;
        }
    }
}
