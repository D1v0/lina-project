﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace LinaProject.Tier3GUI
{
   public class FilesDataInfoGrid
    {
       public Uri viewImage { get; set; }
       [DisplayName("Файл")]
        public string fileName { get; set; }
       [DisplayName("Размер(байт)")]
        public long fileSize { get; set; }
       [DisplayName("Размер(блок)")]
        public uint blockCount { get; set; }
       [DisplayName("Процетн дупликации")]
        public double deduplicationPercent { get; set; }
       public bool empty = true;

        public FilesDataInfoGrid(BaseModels.FileStructConst Constants, uint blockSize)
        {

            this.fileName = Constants.fileName;
            this.fileSize = (Constants.deduplicatedBlockCount + Constants.uniqueBlockCount)*blockSize-(blockSize-Constants.valuedBytes);
            this.blockCount = Constants.deduplicatedBlockCount+Constants.uniqueBlockCount;
            this.deduplicationPercent = Math.Round((double)(Constants.deduplicatedBlockCount * 100) / (Constants.deduplicatedBlockCount + Constants.uniqueBlockCount),2);
            this.empty = false;

            string Extention = "";
            int ind = fileName.Length - 1;
            while (fileName[ind] != '.')
            {
                Extention += fileName[ind];
                ind--;
            }
            IEnumerable<char> bufer = Extention.Reverse();
            Extention = "";
            foreach (var item in bufer)
            {
                Extention += item;
            }
            bool flag = false;
            string pic = ".bmp .cpt .gif .hdr .jpg .jpeg .pcx .pdf .pdn .png .psd .raw .tga .tiff .wdp .xpm .ai .cdr .cgm .eps .pdf .ps .svg .sai .wmf .emf .ani .apng .flc .fli .gif .mng .smil .svg .swf .wlmp";
            string audio = ".aa .aac .amr .ape .asf .cda .flaс .lav .mp3 .mt9 .ogg .voc .wav .wma";
            string video = ".3gp .asf .avi .bik .flv .mkv .mpe .mxf .ogg .mov .qt .rm .avi .smk .swf .vob .wmv .avchd .mts .webm";
            string text = ".txt .guide .rtf .odt .sxw .tex .texi .wpd .doc .docx .docm .lwp";
            string archive = ".7z .ace .arj .bz2 .cab .cpio .deb .f .gz .ha .img .iso .jar .lha .lzh .lzo .lzx .rar .rpm .smc .tar .xz .zip .zoo";
            string internet = ".html .htm .xml .xhtml .xht .maf .mht .mhtml .asp .aspx .adp .bml .cfm .cgi .ihtml .jsp .las .lasso .lassoapp .pl .php .ssi";

            if (Extention == "Папка с файлами" && !flag)
            {
                this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/folder.ico", UriKind.RelativeOrAbsolute);
                flag = true;
            }
            if (pic.Contains(Extention) && !flag)
            {
                this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/Picture.ico", UriKind.RelativeOrAbsolute);
                flag = true;
            }

            if (video.Contains(Extention) && !flag)
            {
                this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/Video.ico", UriKind.RelativeOrAbsolute);
                flag = true;
            }
            if (text.Contains(Extention) && !flag)
            {
                this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/Text.ico", UriKind.RelativeOrAbsolute);
                flag = true;
            }
            if (audio.Contains(Extention) && !flag)
            {
                this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/AudioFile.ico", UriKind.RelativeOrAbsolute);
                flag = true;
            }
            if (internet.Contains(Extention) && !flag)
            {
                this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/Hyperlink.ico", UriKind.RelativeOrAbsolute);
                flag = true;
            }
            if (archive.Contains(Extention) && !flag)
            {
                this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/zippedFile.ico", UriKind.RelativeOrAbsolute);
                flag = true;
            }
            if (!flag)
            {
                this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/OtherFile.ico", UriKind.RelativeOrAbsolute);
            }
        }

       public FilesDataInfoGrid()
        {
            this.fileName = "Недоступно";
            this.fileSize = 0;
            this.blockCount = 0;
            this.deduplicationPercent = 0;
            this.empty = true;
        }
    }
}
