﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseModels;
using System.Collections;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Drawing;

namespace LinaProject.Tier3GUI
{
    public class NewWindowModel : BaseModel
    {

        private FileStructConst _localConst = new FileStructConst();
        public FileStructConst localConst
        {
            get { return _localConst; }
            set { _localConst = value; OnPropertyChanged(() => localConst); }
        }

        private System.Collections.IList _beforeWrite;
        public System.Collections.IList beforeWrite
        {
            get { return _beforeWrite; }
            set { _beforeWrite = value; OnPropertyChanged(() => beforeWrite); }
        }
        

        
        private int _blocCount;
        public int blocCount
        {
            get { return _blocCount; }
            set { _blocCount = value; OnPropertyChanged(() => blocCount); }
        }
        
        

        private string _storageName;
        public string storageName
        {
            get {
                try
                {
                    _storageName = Constants.storageName;
                }
                catch { _storageName = null; }
                if(String.IsNullOrEmpty(_storageName))
                {
                    return "Выберите хранилище";
                }
                return _storageName;
            }
            set { _storageName = value; OnPropertyChanged(() => storageName); }
        }
        

        private byte _encryptionAlg;
        public byte encryptionAlg
        {
            get { return _encryptionAlg; }
            set { _encryptionAlg = value; OnPropertyChanged(() => encryptionAlg); }
        }
        
        

        private bool _create;
        public bool create
        {
            get { return _create; }
            set { _create = value; OnPropertyChanged(() => create); }
        }

        private string _password;
        public string password
        {
            get { return _password; }
            set { _password = value; OnPropertyChanged(() => password); }
        }


        private StorageStructConst _Constants;
        public StorageStructConst Constants
        {
            get { return _Constants; }
            set { _Constants = value; OnPropertyChanged(() => Constants); }
        }

        private string _pathtostorage;
        public string PathToStorage
        {
            get { return _pathtostorage; }
            set { _pathtostorage = value; OnPropertyChanged(() => PathToStorage); }
        }


        private long _storageSize;
        public long storageSize
        {
            get { return _storageSize; }
            set { _storageSize = value; OnPropertyChanged(() => storageSize); }
        }


        private uint _blockSize;
        public uint blockSize
        {
            get { return _blockSize; }
            set { _blockSize = value; OnPropertyChanged(() => blockSize); }
        }



        private string _pathtoread = "ReadPath";
        public string PathToRead
        {
            get { return _pathtoread; }
            set { _pathtoread = value; OnPropertyChanged(() => PathToRead); }
        }

        private string _pathtoextract = "Выберите каталог";
        public string PathToExtract
        {
            get { return _pathtoextract; }
            set { _pathtoextract = value; OnPropertyChanged(() => PathToExtract); }
        }


        private List<FilesDataInfoGrid> _filesInfo = new List<FilesDataInfoGrid>();
        public List<FilesDataInfoGrid> filesInfo
        {
            get { 
               if((_filesInfo.Count==0)||(_filesInfo==null))
               {
                   _filesInfo = new List<FilesDataInfoGrid>();
                   _filesInfo.Add(new FilesDataInfoGrid());
               }
                return _filesInfo; 
            }
            set { _filesInfo = value; OnPropertyChanged(() => filesInfo); }
        }

        public void PropChanged()
        {
            OnPropertyChanged(() => filesText);
        }

        public void GetConstants(StorageStructConst Constants)
        {
            this.Constants = Constants;
        }

        public NewWindowModel()
        {
            prevPath = path;
        }

        private string _path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString();
        public string path
        {
            get { return _path; }
            set { 
                _path = value; OnPropertyChanged(() => path); 
                OnPropertyChanged(() => filesText);
            }
        }

        private string prevPath;

        
        public ObservableCollection<View> filesText
        {
            get
            {             
                try
                {
                    return Reload();
                }
                catch(IOException)
                {
                    MessageBox.Show("Каталог недоступен", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    path = prevPath;
                    return Reload();
                    
                }
                catch(UnauthorizedAccessException)
                {
                    MessageBox.Show("У вас нет прав доступа в этот каталог", "Ошибка!", MessageBoxButtons.OK,MessageBoxIcon.Error);
                    path = prevPath;
                    return Reload();                  
                }
            }
            
        }
         public ObservableCollection<View> Reload()
        {
            ObservableCollection<View> res = new ObservableCollection<View>();
            if (_path == null)
            {
                var driv = Environment.GetLogicalDrives();
                foreach (var elem in driv)
                {
                    res.Add(new View(elem, "", "Логический диск"));
                }
                prevPath = path;
                return res;
            }
            else
            {
                var items = new DirectoryInfo(_path);
                var files = items.GetFiles("*.*");
                var directories = items.GetDirectories();
                res.Add(new View("...", "", ""));
                foreach (var elem in directories)
                {
                    res.Add(new View(elem.Name, "", "Папка с файлами"));
                }
                foreach (var elem in files)
                {
                    res.Add(new View(elem.Name, elem.Length.ToString(), elem.Extension));
                }
                prevPath = path;
                return res;
            }
        }
    }


}



public class View
{
    public Uri viewImage { get; set; }
    [DisplayName("Имя файла")]
    public string Name { get; set; }
    [DisplayName("Размер (байт)"), ReadOnly(true)]
    public string Size { get; set; }
    [DisplayName("Расширение файла"), ReadOnly(true)]
    public string Extention { get; set; }
    public View(string Name, string Size, string Extention)
    {
        bool flag = false;
        this.Name = Name;
        this.Size = Size;
        this.Extention = Extention;
        string pic = ".bmp .cpt .gif .hdr .jpg .jpeg .pcx .pdf .pdn .png .psd .raw .tga .tiff .wdp .xpm .ai .cdr .cgm .eps .pdf .ps .svg .sai .wmf .emf .ani .apng .flc .fli .gif .mng .smil .svg .swf .wlmp";
        string audio = ".aa .aac .amr .ape .asf .cda .flaс .lav .mp3 .mt9 .ogg .voc .wav .wma";
        string video = ".3gp .asf .avi .bik .flv .mkv .mpe .mxf .ogg .mov .qt .rm .avi .smk .swf .vob .wmv .avchd .mts .webm";
        string text = ".txt .guide .rtf .odt .sxw .tex .texi .wpd .doc .docx .docm .lwp";
        string archive = ".7z .ace .arj .bz2 .cab .cpio .deb .f .gz .ha .img .iso .jar .lha .lzh .lzo .lzx .rar .rpm .smc .tar .xz .zip .zoo";
        string internet = ".html .htm .xml .xhtml .xht .maf .mht .mhtml .asp .aspx .adp .bml .cfm .cgi .ihtml .jsp .las .lasso .lassoapp .pl .php .ssi";

        if (Name == "...")
        {
            this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/Up.ico", UriKind.RelativeOrAbsolute);
            flag = true;
        }
        if (Extention == "Папка с файлами" && !flag)
        {
            this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/folder.ico", UriKind.RelativeOrAbsolute);
            flag = true;
        }
        if (pic.Contains(Extention) && !flag)
        {
            this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/Picture.ico", UriKind.RelativeOrAbsolute);
            flag = true;
        }

        if (video.Contains(Extention) && !flag)
        {
            this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/Video.ico", UriKind.RelativeOrAbsolute);
            flag = true;
        }
        if (text.Contains(Extention) && !flag)
        {
            this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/Text.ico", UriKind.RelativeOrAbsolute);
            flag = true;
        }
        if (audio.Contains(Extention) && !flag)
        {
            this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/AudioFile.ico", UriKind.RelativeOrAbsolute);
            flag = true;
        }
        if(internet.Contains(Extention)&& !flag)
        {
            this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/Hyperlink.ico", UriKind.RelativeOrAbsolute);
            flag = true;
        }
        if(archive.Contains(Extention) && !flag)
        {
            this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/zippedFile.ico", UriKind.RelativeOrAbsolute);
            flag = true;
        }
       if(!flag)
        {
            this.viewImage = new Uri("pack://application:,,,/LinaProject;component/Resources/OtherFile.ico", UriKind.RelativeOrAbsolute);
        }
    }
}
