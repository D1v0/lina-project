﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;

namespace LinaProject.Tier2Crypto
{
    abstract class AbstractCrypto : BaseModels.BaseModel, CryptoInterface
    {
        abstract public byte[] Encrypt(byte[] Block);

        abstract public byte[] Decrypt(byte[] Block);
    }
}
