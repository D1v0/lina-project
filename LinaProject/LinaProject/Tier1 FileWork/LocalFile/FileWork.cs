﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
using BaseModels;
using System.IO;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;
using System.Windows.Forms;



namespace LinaProject.Tier1_FileWork.LocalFile
{
    public class FileWork : BaseModel, FileInterface
    {

        private long _stblcsz = 4096;
        public long standartBlockSize
        {
            get { return _stblcsz; }
            set { _stblcsz = value; OnPropertyChanged(() => standartBlockSize); }
        }

        public struct InfoAboutFile
        {
            public char[] Name;
            public long Size;

            public InfoAboutFile(FileInfo Info)
            {
                Name = new char[256];
                for (int i = 0; i < Info.Name.Length; i++)
                {
                    Name[i] = Info.Name[i];
                }
                for (int i = Info.Name.Length; i < Name.Length; i++)
                {
                    Name[i] = '-';
                }
                Size = Info.Length;


            }
        }

        private FileInfo info;
        public FileInfo Info
        {
            get
            {
                info = new FileInfo(Path);
                return info;
            }
            set { info = value; OnPropertyChanged(() => Info); }
        }

        private FileStream stream;
        public FileStream Stream
        {
            get
            {
                return stream;
            }
            set { stream = value; OnPropertyChanged(() => Stream); }
        }

        private string _path = "Path Here";
        public string Path
        {
            get { return _path; }
            set { _path = value; OnPropertyChanged(() => Path); }
        }

        

        public FileWork(string Path)
        {
            this.Path = Path;
            try
            {
                Stream = File.OpenRead(Path);
            }
            catch (FileNotFoundException e)
            {
                MessageBox.Show("Такого файла нет", "Ошибка");
            }
        }

        public void Close()//использовать в случае не вызова метода Read()
        {
            Stream.Close();
        }

        public byte[] Read(uint standartBlockSize)
        {
            byte[] result = new byte[standartBlockSize];
            try
            {
                Stream.Read(result, 0, result.Length);
                return result;
            }
            catch { return null; }
        }

       




      /* public struct InfoAboutFile
        {
          public  char[] Name;
          public  long Size;

          public InfoAboutFile(FileInfo Info)
          {
              Name = new char[256];
              for (int i = 0; i < Info.Name.Length; i++)
              {
                  Name[i] = Info.Name[i];
              }
              Size = Info.Length;

          }
        }

        private FileInfo info;
        public FileInfo Info
        {
            get
            {
                info = new FileInfo(Path);
                return info;
            }
            set { info = value; OnPropertyChanged(() => Info); }
        }

        private SafeFileHandle _handle;
        public SafeFileHandle Handle
        {
            get { return _handle; }
            set { _handle = value; OnPropertyChanged(() => Handle); }
        }
        private Int64 _size;
        public Int64 Size
        {
            get { return _size; }
            set { _size = value; OnPropertyChanged(() => Size); }
        }
        private string _path = "Path Here";
        public string Path
        {
            get { return _path; }
            set { _path = value; OnPropertyChanged(() => Path); }
        }

        public FileWork(string Path)
            :base(Path)
        {
            this.Path = Path;
           
        }

        bool FileInterface.Open(string Path)
        {
            try
            {
                UnmanagedFileLoader ufl = new UnmanagedFileLoader(Path);
                ufl.Load(Path);
                Handle = ufl.Handle;           
                return true;
            }
            catch (Exception e)
            { return false; }
        }

        bool FileInterface.Create(string Path)
        {
            try
            {
                UnmanagedFileLoader ufl = new UnmanagedFileLoader(Path);
                ufl.Create(Path);
                Handle = ufl.Handle;
                return true;

            }
            catch (Exception e)
            {
                return false;
            }
        }
        bool FileInterface.Close(SafeFileHandle Handle)
        {
            try
            {
                Handle.Dispose();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        Dictionary<int, byte[]> FileInterface.Read(SafeFileHandle Handle)
        {
            FileStream diskStreamToRead = new FileStream(Handle, FileAccess.Read);
            
            Dictionary<int, byte[]> buffer = new Dictionary<int, byte[]>();
            int key = 1;
             for (int i = 0; i < Math.Floor((double)diskStreamToRead.Length/4096); i++)
             {
                 try
                 {
                     byte[] buf = new byte[4096];
                     diskStreamToRead.ReadAsync(buf, 0, buf.Length);
                     buffer.Add(key, buf);
                     key++;
                 }
                 catch { break; }
             }
            try { diskStreamToRead.Close(); }
            catch { }
            try { Handle.Close(); }
            catch { }
            return buffer;
        }

        bool FileInterface.Write(SafeFileHandle Handle, Dictionary<int, byte[]> dataToWrite, FileInfo Info)
        {
            FileStream diskStreamToWrite = new FileStream(Handle, FileAccess.Write);
            InfoAboutFile inf = new InfoAboutFile(Info);
            byte[] name = new byte[4096];
            byte[] size = new byte[4096];
            Encoding enc = Encoding.UTF8;
            name = enc.GetBytes(inf.Name);
            size = BitConverter.GetBytes(inf.Size);
            diskStreamToWrite.Write(name, 0, name.Length);
            diskStreamToWrite.Write(size, 0, size.Length);
            foreach (var item in dataToWrite)
                {
                    diskStreamToWrite.Write(item.Value, 0, item.Value.Length);
                  
                }

            try { diskStreamToWrite.Close(); return true; }
            catch { return false; }
            try { Handle.Close(); return true; }
            catch { return false; }
        }*/



       
    }
}
