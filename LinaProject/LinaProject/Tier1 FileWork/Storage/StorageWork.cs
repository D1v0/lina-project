﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
using BaseModels;
using System.IO;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Security.Cryptography;
using LinaProject.Tier2Crypto;

namespace LinaProject.Tier1_FileWork.Storage
{
    class StorageWork : AbstractCrypto, FileInterface
    {

        public StorageStructConst Constatns;
        private FileStream stream;
        public FileStream Stream
        {
            get
            {
                return stream;
            }
            set { stream = value; OnPropertyChanged(() => Stream); }
        }

        private string _path = "Path Here";
        public string Path
        {
            get { return _path; }
            set { _path = value; OnPropertyChanged(() => Path); }
        }

        public StorageStructConst Open(string Path)
        {
            try
            {
                Stream = File.Open(Path, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                this.Path = Path;
                return Load(Stream);
            }
            catch { return null; }
        }

        public StorageStructConst OpenWeak(string Path)
        {
            try
            {
                Stream = File.Open(Path, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                this.Path = Path;
                return GetWeakConstants(Stream);
            }
            catch { return null; }
        }

        public StorageStructConst CreateNewStorage(string Path, long Size, uint blockSize, string Password, byte encryptionAlg)
        {
            try
            {
                this.Path = Path;
                //держим поток и не закрываем его
                Constatns = new StorageStructConst();
                Stream = File.Create(Path, (int)blockSize, FileOptions.Asynchronous);
                return FillNew(Stream, Size, blockSize, Password, encryptionAlg);
            }
            catch
            {
                MessageBox.Show("Ошибка при создании хранилища. Обратитеть к создателю.", "Ошибка");
                return null;
            }
        }



        public bool DeleteStorage(Stream stream)
        {
            try
            {
                Stream.Close();
                File.Delete(Path);
                return true;
            }
            catch { return false; }
        }

        public StorageStructConst FillNew(FileStream Stream, long Size, uint blockSize, string Password, byte encryptinoAlg)
        {
            try
            {
                Constatns.standartBlockSize = blockSize;

                byte[] blocks = new byte[Constatns.standartBlockSize];
                Constatns.blockCount = (uint)Math.Floor(Convert.ToDouble(Size / Constatns.standartBlockSize));
                Stream.SetLength(Size);
                /*for (int i = 0; i < Constatns.blockCount; i++ )
                {
                    Stream.Write(blocks, 0, blocks.Length);
                }*/
                Stream.Position = 0;
                Encoding enc = Encoding.UTF8;
                string parse_name = "";
                int ind = Stream.Name.Length - 1;
                while (Stream.Name[ind] != '\\')
                {
                    parse_name += Stream.Name[ind];
                    ind--;
                }
                IEnumerable<char> bufer = parse_name.Reverse();
                parse_name = "";
                foreach (var item in bufer)
                {
                    parse_name += item;
                }

                Constatns.storageName = parse_name; //64*sizeof(char)
                Constatns.storageSize = Size;
                //  Constatns.isFirstWrite = true;
                Constatns.dateTime = Convert.ToString(DateTime.Now); //64*sizeof(char);
                Constatns.fillBlocks = 0; //sizeof(uint)
                Constatns.indexFreeBlocks = new int[Constatns.blockCount];
                Constatns.structSize = new uint[Constatns.blockCount];
                //Constatns.indexFreeBlocks = new uint[(uint)Math.Ceiling((double)(Constatns.blockCount / 256)), 1024 / 4]; //256*(uint)Math.Ceiling((double)(Constatns.blockCount/256))*4
                //Constatns.structSize = new uint[(uint)Math.Ceiling((double)(Constatns.blockCount / 256)), 1024 / 4];  //256*(uint)Math.Ceiling((double)(Constatns.blockCount/256))*4
                Constatns.serviceStructSize = sizeof(uint) + 64 * sizeof(char) + sizeof(long) + 64 * sizeof(char) + sizeof(uint)
                    + sizeof(uint) + sizeof(uint) + Constatns.blockCount * sizeof(uint) + Constatns.blockCount * sizeof(uint)
                    + sizeof(uint) + 64 * sizeof(char);
                Constatns.freeBlocks = Constatns.blockCount - (uint)Math.Ceiling((double)Constatns.serviceStructSize / Constatns.standartBlockSize);    //sizeof(uint)
                Constatns.encryptionAlg = encryptinoAlg;
                char[] c_name = new char[64 * sizeof(char)];
                char[] c_date = new char[64 * sizeof(char)];
                byte[] b_name = new byte[64 * sizeof(char)];
                for (int i = 0; i < (Encoding.UTF8).GetBytes(Constatns.storageName).Length; i++)
                {
                    b_name[i] = (Encoding.UTF8).GetBytes(Constatns.storageName)[i];
                }
                int sind = (Encoding.UTF8).GetBytes(Constatns.storageName).Length;
                while (sind < 64 * sizeof(char))
                {
                    b_name[sind] = 63;
                    sind++;
                }

                for (int i = 0; i < Constatns.dateTime.Length; i++)
                {
                    c_date[i] = Constatns.dateTime[i];
                }
                for (int i = Constatns.dateTime.Length; i < c_date.Length; i++)
                {
                    c_date[i] = '?';
                }
                Stream.Write(BitConverter.GetBytes(Constatns.standartBlockSize), 0, BitConverter.GetBytes(Constatns.standartBlockSize).Length);
                Stream.Write(b_name, 0, b_name.Length);
                // Stream.Write(enc.GetBytes(c_name), 0, enc.GetBytes(c_name).Length);
                Stream.Write(BitConverter.GetBytes(Constatns.storageSize), 0, BitConverter.GetBytes(Constatns.storageSize).Length);
                //    Stream.Write(BitConverter.GetBytes(Constatns.isFirstWrite), 0, BitConverter.GetBytes(Constatns.isFirstWrite).Length);
                Stream.Write(enc.GetBytes(c_date), 0, enc.GetBytes(c_date).Length);
                Stream.Write(BitConverter.GetBytes(Constatns.blockCount), 0, BitConverter.GetBytes(Constatns.blockCount).Length);
                Stream.Write(BitConverter.GetBytes(Constatns.fillBlocks), 0, BitConverter.GetBytes(Constatns.fillBlocks).Length);
                Stream.Write(BitConverter.GetBytes(Constatns.freeBlocks), 0, BitConverter.GetBytes(Constatns.freeBlocks).Length);


                byte[] b_structSize = new byte[Constatns.blockCount * 4];
                Stream.Write(b_structSize, 0, b_structSize.Length);
                Stream.Write(b_structSize, 0, b_structSize.Length);
                Stream.Write(BitConverter.GetBytes(Constatns.serviceStructSize), 0, BitConverter.GetBytes(Constatns.serviceStructSize).Length);
                Constatns.password = new byte[64 * sizeof(char)];
                Constatns.password = EncryptPass(Password);
                Password = null;
                Stream.Write(Constatns.password, 0, Constatns.password.Length);
                Stream.Write(BitConverter.GetBytes(encryptinoAlg), 0, sizeof(byte));
                Stream.Position = 0;
                return Constatns;
            }
            catch { return null; }
        }

        public StorageStructConst Load(FileStream Stream)
        {
            Constatns = new StorageStructConst();
            Stream.Position = 0;
            Encoding enc = Encoding.UTF8;
            try
            {
                byte[] uint_array = new byte[sizeof(uint)];
                byte[] b_string_array = new byte[64 * sizeof(char)];
                char[] string_array = new char[64 * sizeof(char)];
                Stream.Read(uint_array, 0, uint_array.Length);
                Constatns.standartBlockSize = (uint)BitConverter.ToInt32(uint_array, 0);
                Stream.Read(b_string_array, 0, b_string_array.Length);
                string_array = enc.GetChars(b_string_array);
                int ind = 0;
                while (string_array[ind] != '?')
                {
                    Constatns.storageName += string_array[ind];
                    ind++;
                }

                byte[] long_array = new byte[sizeof(long)];
                Stream.Read(long_array, 0, long_array.Length);
                Constatns.storageSize = (long)BitConverter.ToInt64(long_array, 0);
                b_string_array = new byte[64 * sizeof(char)];
                string_array = new char[64 * sizeof(char)];
                Stream.Read(b_string_array, 0, b_string_array.Length);
                string_array = enc.GetChars(b_string_array);
                ind = 0;
                while (string_array[ind] != '?')
                {
                    Constatns.dateTime += string_array[ind];
                    ind++;
                }
                uint_array = new byte[sizeof(uint)];
                Stream.Read(uint_array, 0, uint_array.Length);
                Constatns.blockCount = (uint)BitConverter.ToInt32(uint_array, 0);
                uint_array = new byte[sizeof(uint)];
                Stream.Read(uint_array, 0, uint_array.Length);
                Constatns.fillBlocks = (uint)BitConverter.ToInt32(uint_array, 0);
                uint_array = new byte[sizeof(uint)];
                Stream.Read(uint_array, 0, uint_array.Length);
                Constatns.freeBlocks = (uint)BitConverter.ToInt32(uint_array, 0);
                Constatns.indexFreeBlocks = new int[Constatns.blockCount];
                Constatns.structSize = new uint[Constatns.blockCount];
                try
                {
                    for (int i = 0; i < Constatns.blockCount; i++)
                    {
                        uint_array = new byte[sizeof(uint)];
                        Stream.Read(uint_array, 0, uint_array.Length);
                        Constatns.indexFreeBlocks[i] = (int)BitConverter.ToInt32(uint_array, 0);
                    }
                }
                catch { return null; }
                for (uint i = 0; i < Constatns.blockCount; i++)
                {
                    uint_array = new byte[sizeof(uint)];
                    Stream.Read(uint_array, 0, uint_array.Length);
                    Constatns.structSize[i] = (uint)BitConverter.ToInt32(uint_array, 0);
                }
                uint_array = new byte[sizeof(uint)];
                Stream.Read(uint_array, 0, uint_array.Length);
                Constatns.serviceStructSize = (uint)BitConverter.ToInt32(uint_array, 0);
                b_string_array = new byte[64 * sizeof(char)];
                Stream.Read(b_string_array, 0, b_string_array.Length);
                Constatns.password = b_string_array;
                byte[] buf = new byte[sizeof(byte)];
                Stream.Read(buf,0,buf.Length);
                Constatns.encryptionAlg = buf[0];
                return Constatns;
            }
            catch { return null; }
        }

        public StorageStructConst GetWeakConstants(FileStream Stream)
        {
            Constatns = new StorageStructConst();
            Stream.Position = 0;
            Encoding enc = Encoding.UTF8;
            try
            {
                byte[] uint_array = new byte[sizeof(uint)];
                byte[] long_array = new byte[sizeof(long)];
                Stream.Read(uint_array, 0, uint_array.Length);
                Constatns.standartBlockSize = (uint)BitConverter.ToInt32(uint_array, 0);

                Stream.Position += 64 * sizeof(char);

                Stream.Read(long_array, 0, long_array.Length);
                Constatns.storageSize = BitConverter.ToInt64(long_array, 0);
                Stream.Position += 64 * sizeof(char);
                uint_array = new byte[sizeof(uint)];
                Stream.Read(uint_array, 0, uint_array.Length);
                Constatns.blockCount = (uint)BitConverter.ToInt32(uint_array, 0);

                Stream.Position += 2 * sizeof(uint) + 2 * sizeof(uint) * Constatns.blockCount + sizeof(uint);


                byte[] b_string_array = new byte[64 * sizeof(char)];
                Stream.Read(b_string_array, 0, b_string_array.Length);
                Constatns.password = b_string_array;
                return Constatns;
            }
            catch { return null; }

        }

        public FileStructConst CreateDepletedFileStruct(FileInfo Info, uint blockCount)
        {
            FileStructConst fileConstants = new FileStructConst();
            fileConstants.structSize = sizeof(uint) + 64 * sizeof(char) + sizeof(uint) + sizeof(uint) + sizeof(uint)
                + blockCount * sizeof(uint) + blockCount * sizeof(int) + sizeof(uint) + sizeof(bool);
            uint preSize = (uint)Math.Ceiling((double)fileConstants.structSize / Constatns.standartBlockSize);
            if (preSize + blockCount >= Constatns.freeBlocks)
            {
                return null;
            }
            try
            {
                fileConstants.usedBlocks = new uint[blockCount];
                fileConstants.structOffset = Constatns.serviceStructSize;
                //Цикл по структурам, поиск свободной позиции.
                int ind = Array.IndexOf(Constatns.structSize, (uint)0);
                if (ind != -1)
                {
                    var Summ = Constatns.structSize.Take(ind + 1).Aggregate((av, e) => av + e);
                    fileConstants.structOffset = Summ;
                    fileConstants.Nubmer = (uint)ind + 1;
                    fileConstants.SHA1Table = new int[blockCount];
                }
                else
                {
                    return null;
                }
                char[] c_name = new char[64 * sizeof(char)];
                byte[] b_name = new byte[64 * sizeof(char)];
                for (int i = 0; i < (Encoding.UTF8).GetBytes(Info.Name).Length; i++)
                {
                    b_name[i] = (Encoding.UTF8).GetBytes(Info.Name)[i];
                }
                ind = (Encoding.UTF8).GetBytes(Info.Name).Length;
                while (ind < 64 * sizeof(char))
                {
                    b_name[ind] = 63;
                    ind++;
                }
                fileConstants.fileName = (Encoding.UTF8).GetString(b_name);
                fileConstants.valuedBytes = (uint)(Constatns.standartBlockSize -
                    (blockCount * Constatns.standartBlockSize - Info.Length));
                fileConstants.onDelete = false;
                return fileConstants;
            }
            catch { return null; }
        }

        public bool WriteEnrichedFileStruct(FileStructConst fileConstants)
        {
            uint offset = Constatns.serviceStructSize;
            Encoding enc = Encoding.UTF8;
            int ind = Array.IndexOf(Constatns.structSize, (uint)0);
            var Summ = Constatns.structSize.Take(ind + 1).Aggregate((av, e) => av + e);
            offset += Summ;
            if (ind == -1)
            {
                return false;
            }
            else
            {
                byte[] structField = new byte[sizeof(uint)];
                byte[] b_name = new byte[64 * sizeof(char)];
                Stream.Position = offset;
                Stream.Write(BitConverter.GetBytes(fileConstants.Nubmer), 0, BitConverter.GetBytes(fileConstants.Nubmer).Length);
                byte[] buf = enc.GetBytes(fileConstants.fileName);
                Stream.Write(buf, 0, buf.Length);
                Stream.Write(BitConverter.GetBytes(fileConstants.structSize), 0, BitConverter.GetBytes(fileConstants.structSize).Length);
                Stream.Write(BitConverter.GetBytes(fileConstants.deduplicatedBlockCount), 0, BitConverter.GetBytes(fileConstants.deduplicatedBlockCount).Length);
                Stream.Write(BitConverter.GetBytes(fileConstants.uniqueBlockCount), 0, BitConverter.GetBytes(fileConstants.uniqueBlockCount).Length);
                for (int k = 0; k < fileConstants.uniqueBlockCount + fileConstants.deduplicatedBlockCount; k++)
                {
                    Stream.Write(BitConverter.GetBytes(fileConstants.usedBlocks[k]), 0, BitConverter.GetBytes(fileConstants.usedBlocks[k]).Length);
                }
                for (int k = 0; k < fileConstants.uniqueBlockCount + fileConstants.deduplicatedBlockCount; k++)
                {
                    Stream.Write(BitConverter.GetBytes(fileConstants.SHA1Table[k]), 0, sizeof(int));
                }
                Stream.Write(BitConverter.GetBytes(fileConstants.valuedBytes), 0, BitConverter.GetBytes(fileConstants.valuedBytes).Length);
                Stream.Write(BitConverter.GetBytes(fileConstants.onDelete), 0, BitConverter.GetBytes(fileConstants.onDelete).Length);
                uint pre_fill = (uint)Math.Ceiling(((double)fileConstants.structSize) / Constatns.standartBlockSize);
                Constatns.freeBlocks -= pre_fill;
                Constatns.structSize[ind] = fileConstants.structSize;
                return true;
            }
        }

        public uint IsDeduplicated(uint[] writenBlocks, byte[] Block)
        {

            byte[] buff = new byte[Constatns.standartBlockSize];
            int i = 0;
            while (i != writenBlocks.Length)
            {
                //
                if (writenBlocks[i] == 0)
                {
                    i++;
                }
                else
                {
                    Stream.Position = Constatns.storageSize -
                        Constatns.standartBlockSize * writenBlocks[i];// - Constatns.standartBlockSize;
                    Stream.Read(buff, 0, buff.Length);
                    bool eq = buff.SequenceEqual(Block);
                    if (eq)
                    {
                        return writenBlocks[i];
                    }
                    else { i++; }
                }
            }
            return 0;
        }

        /// <summary>
        /// Обычная запись блока, с проверкой на дуплекацию.
        /// Цикл создается выше(т.е выше уровнем).
        /// </summary>
        /// <param name="Block"></param>
        /// <param name="fileConstants"></param>
        /// <param name="page"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public FileStructConst WriteBlock(byte[] Block, uint blockCount, FileStructConst fileConstants)
        {
           
            SHA1 sh = new SHA1Managed();
            int hash = BitConverter.ToInt32(sh.ComputeHash(Block), 0);
            int index = Array.IndexOf(Constatns.indexFreeBlocks, hash);
            if (index != -1)
            {
                fileConstants.deduplicatedBlockCount++;
                fileConstants.usedBlocks[fileConstants.localIndex] = (uint)index + 1;
                fileConstants.SHA1Table[fileConstants.localIndex] = hash;
                fileConstants.localIndex++;
                return fileConstants;
            }
            else
            {
                int firstEmpty = Array.IndexOf(Constatns.indexFreeBlocks, 0);
                if (firstEmpty != -1)
                {
                    fileConstants.uniqueBlockCount++;
                    fileConstants.usedBlocks[fileConstants.localIndex] = (uint)firstEmpty + 1;
                    fileConstants.SHA1Table[fileConstants.localIndex] = hash;
                    Constatns.freeBlocks--;
                    Constatns.fillBlocks++;
                    Constatns.indexFreeBlocks[firstEmpty] = hash;
                    Stream.Position = (Constatns.storageSize - (firstEmpty + 1) * Constatns.standartBlockSize);
                    Stream.Write(Encrypt(Block), 0, Block.Length);
                    fileConstants.localIndex++;
                    return fileConstants;
                }
                else
                {
                    return null;
                }
            }
        }

        public FileStructConst WriteBlockFromStream(Stream inputStream,  FileStructConst fileConstants)
        {
            SHA1 sh = new SHA1Managed();
            byte[] readenFile = new byte[inputStream.Length];
            inputStream.Read(readenFile, 0, readenFile.Length);
            int blockCount = (int)Math.Ceiling((double)(readenFile.Length / Constatns.standartBlockSize));
            List<byte[]> dict = new List<byte[]>();
            for (int i = 0; i < blockCount; i++)
            {
                dict.Add(readenFile.Skip(i * 64).Take(64).ToArray());
            }
            foreach (var elem in dict)
            {
                int hash = BitConverter.ToInt32(sh.ComputeHash(elem), 0);
                int index = Array.IndexOf(Constatns.indexFreeBlocks, hash);
                if (index != -1)
                {
                    fileConstants.deduplicatedBlockCount++;
                    fileConstants.usedBlocks[fileConstants.localIndex] = (uint)index + 1;
                    fileConstants.SHA1Table[fileConstants.localIndex] = hash;
                    fileConstants.localIndex++;                   
                }
                else
                {
                    int firstEmpty = Array.IndexOf(Constatns.indexFreeBlocks, 0);
                    if (firstEmpty != -1)
                    {
                        fileConstants.uniqueBlockCount++;
                        fileConstants.usedBlocks[fileConstants.localIndex] = (uint)firstEmpty + 1;
                        fileConstants.SHA1Table[fileConstants.localIndex] = hash;
                        Constatns.freeBlocks--;
                        Constatns.fillBlocks++;
                        Constatns.indexFreeBlocks[firstEmpty] = hash;
                        Stream.Position = (Constatns.storageSize - (firstEmpty + 1) * Constatns.standartBlockSize);
                        Stream.Write(elem, 0, elem.Length);
                        fileConstants.localIndex++;
                        return fileConstants;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return fileConstants;
        }

        public bool Exctract(string Path, string Name)
        {
            FileStream writenFile;
            long position;
            try
            {
                writenFile = File.Create(Path + "\\" + Name);
            }
            catch { return false; }
            Encoding enc = Encoding.UTF8;
            uint deduplicatedBlockCount = 0, uniqueBlockCount = 0;
            uint offset = Constatns.serviceStructSize;
            char[] c_name = new char[64 * sizeof(char)];
            byte[] bc_name = new byte[64 * sizeof(char)];
            char[] s_name = new char[64 * sizeof(char)];
            byte[] b_name = new byte[64 * sizeof(char)];
            /*for (int i = 0; i < Name.Length; i++)
            {
                c_name[i] = Name[i];
            }
            for (int i = Name.Length; i < c_name.Length; i++)
            {
                c_name[i] = '?';
            }*/
            for (int i = 0; i < (Encoding.UTF8).GetBytes(Name).Length; i++)
            {
                bc_name[i] = (Encoding.UTF8).GetBytes(Name)[i];
            }
            int ind = (Encoding.UTF8).GetBytes(Name).Length;
            while (ind < 64 * sizeof(char))
            {
                bc_name[ind] = 63;
                ind++;
            }
            c_name = enc.GetChars(bc_name);

            for (int i = 0; i < Constatns.blockCount; i++)
            {
                if (Constatns.structSize[i] != 0)
                {
                    byte[] buffer = new byte[sizeof(uint)];
                    Stream.Position = offset + sizeof(uint);
                    Stream.Read(b_name, 0, b_name.Length);
                    s_name = enc.GetChars(b_name);
                    if (s_name.SequenceEqual(c_name))
                    {
                        buffer = new byte[sizeof(uint)];
                        Stream.Position += sizeof(uint);
                        Stream.Read(buffer, 0, buffer.Length);
                        deduplicatedBlockCount = (uint)BitConverter.ToInt32(buffer, 0);
                        buffer = new byte[sizeof(uint)];
                        Stream.Read(buffer, 0, buffer.Length);
                        uniqueBlockCount = (uint)BitConverter.ToInt32(buffer, 0);
                        buffer = new byte[sizeof(uint)];
                        byte[] Block = new byte[Constatns.standartBlockSize];
                        uint blockRef;

                        for (uint k = 0; k < deduplicatedBlockCount + uniqueBlockCount - 1; k++)
                        {

                            Block = new byte[Constatns.standartBlockSize];
                            Stream.Read(buffer, 0, buffer.Length);
                            blockRef = (uint)BitConverter.ToInt32(buffer, 0);
                            position = Stream.Position;
                            Stream.Position = Constatns.storageSize - blockRef * Constatns.standartBlockSize;
                            Stream.Read(Block, 0, Block.Length);
                            writenFile.Write(Decrypt(Block), 0, Block.Length);
                            Stream.Position = position;

                        }
                        Stream.Read(buffer, 0, buffer.Length);
                        blockRef = (uint)BitConverter.ToInt32(buffer, 0);
                        Stream.Position += (deduplicatedBlockCount + uniqueBlockCount) * sizeof(int);
                        Stream.Read(buffer, 0, buffer.Length);
                        uint valuedBytes = (uint)BitConverter.ToInt32(buffer, 0);

                        Stream.Position = Constatns.storageSize - blockRef * Constatns.standartBlockSize;
                        Stream.Read(Block, 0, Block.Length);
                        byte[] decBlock = Block;
                        Block = new byte[valuedBytes];
                        Block = decBlock.Take((int)valuedBytes).ToArray();
                        writenFile.Write(Decrypt(Block), 0, Block.Length);
                        Stream.Position = 0;
                        writenFile.Close();
                        return true;
                    }
                    else
                    {
                        offset += Constatns.structSize[i];
                        Stream.Position = offset;
                    }

                }
                else
                {
                    writenFile.Close();
                    return false;
                }

            }
            writenFile.Close();
            return false;

        }

        public void ReFill()
        {
            try
            {

                Stream.Position = sizeof(uint) + 64 * sizeof(char) + sizeof(long) + 64 * sizeof(char) + sizeof(uint);

                Stream.Write(BitConverter.GetBytes(Constatns.fillBlocks), 0, BitConverter.GetBytes(Constatns.fillBlocks).Length);
                Stream.Write(BitConverter.GetBytes(Constatns.freeBlocks), 0, BitConverter.GetBytes(Constatns.freeBlocks).Length);
                for (int i = 0; i < Constatns.blockCount; i++)
                {
                    Stream.Write(BitConverter.GetBytes(Constatns.indexFreeBlocks[i]), 0, sizeof(int));
                }
                for (int i = 0; i < Constatns.blockCount; i++)
                {
                    Stream.Write(BitConverter.GetBytes(Constatns.structSize[i]), 0, sizeof(uint));
                }
                Stream.Position = 0;
            }
            catch { }
        }

        public FileStructConst LoadFileStruct(long Position)
        {

            FileStructConst fileConstants = new FileStructConst();
            Encoding enc = Encoding.UTF8;
            byte[] buffer;
            byte[] b_string_array = new byte[64 * sizeof(char)];
            char[] string_array = new char[64 * sizeof(char)];
            try
            {
                buffer = new byte[sizeof(uint)];
                Stream.Position = Position;
                Stream.Read(buffer, 0, buffer.Length);
                fileConstants.Nubmer = (uint)BitConverter.ToInt32(buffer, 0);
                Stream.Read(b_string_array, 0, b_string_array.Length);
                string_array = enc.GetChars(b_string_array, 0, b_string_array.Length);
                int ind = 0;
                while (string_array[ind] != '?')
                {
                    fileConstants.fileName += string_array[ind];
                    ind++;
                }
                Stream.Read(buffer, 0, buffer.Length);
                fileConstants.structSize = (uint)BitConverter.ToInt32(buffer, 0);
                Stream.Read(buffer, 0, buffer.Length);
                fileConstants.deduplicatedBlockCount = (uint)BitConverter.ToInt32(buffer, 0);
                Stream.Read(buffer, 0, buffer.Length);
                fileConstants.uniqueBlockCount = (uint)BitConverter.ToInt32(buffer, 0);
                fileConstants.usedBlocks = new uint[fileConstants.deduplicatedBlockCount + fileConstants.uniqueBlockCount];
                for (int i = 0; i < fileConstants.deduplicatedBlockCount + fileConstants.uniqueBlockCount; i++)
                {
                    buffer = new byte[sizeof(uint)];
                    Stream.Read(buffer, 0, buffer.Length);
                    fileConstants.usedBlocks[i] = (uint)BitConverter.ToInt32(buffer, 0);
                }
                fileConstants.SHA1Table = new int[fileConstants.deduplicatedBlockCount + fileConstants.uniqueBlockCount];
                for (int i = 0; i < fileConstants.deduplicatedBlockCount + fileConstants.uniqueBlockCount; i++)
                {
                    buffer = new byte[sizeof(uint)];
                    Stream.Read(buffer, 0, buffer.Length);
                    fileConstants.SHA1Table[i] = BitConverter.ToInt32(buffer, 0);
                }
                Stream.Read(buffer, 0, buffer.Length);
                fileConstants.valuedBytes = (uint)BitConverter.ToInt32(buffer, 0);
                buffer = new byte[sizeof(bool)];
                Stream.Read(buffer, 0, buffer.Length);
                fileConstants.onDelete = BitConverter.ToBoolean(buffer, 0);
                return fileConstants;
            }
            catch { return null; }
        }
        /// <summary>
        /// Выполняет простую запись структуры на указанную позицию Не меняет служебную структуру хранилища.
        /// </summary>
        /// <param name="Position"></param>
        /// <param name="fileConstants"></param>
        /// <returns></returns>
        private bool WriteFileStruct(long Position, FileStructConst fileConstants)
        {
            Encoding enc = Encoding.UTF8;
            byte[] structField = new byte[sizeof(uint)];
            byte[] b_name = new byte[64 * sizeof(char)];

            Stream.Position = Position;
            Stream.Write(BitConverter.GetBytes(fileConstants.Nubmer), 0, BitConverter.GetBytes(fileConstants.Nubmer).Length);
            for (int i = 0; i < (Encoding.UTF8).GetBytes(fileConstants.fileName).Length; i++)
            {
                b_name[i] = (Encoding.UTF8).GetBytes(fileConstants.fileName)[i];
            }
            int ind = (Encoding.UTF8).GetBytes(fileConstants.fileName).Length;
            while (ind < 64 * sizeof(char))
            {
                b_name[ind] = 63;
                ind++;
            }
            Stream.Write(b_name, 0, b_name.Length);
            Stream.Write(BitConverter.GetBytes(fileConstants.structSize), 0, BitConverter.GetBytes(fileConstants.structSize).Length);
            Stream.Write(BitConverter.GetBytes(fileConstants.deduplicatedBlockCount), 0, BitConverter.GetBytes(fileConstants.deduplicatedBlockCount).Length);
            Stream.Write(BitConverter.GetBytes(fileConstants.uniqueBlockCount), 0, BitConverter.GetBytes(fileConstants.uniqueBlockCount).Length);
            for (int k = 0; k < fileConstants.uniqueBlockCount + fileConstants.deduplicatedBlockCount; k++)
            {
                Stream.Write(BitConverter.GetBytes(fileConstants.usedBlocks[k]), 0, BitConverter.GetBytes(fileConstants.usedBlocks[k]).Length);
            }
            for (int k = 0; k < fileConstants.uniqueBlockCount + fileConstants.deduplicatedBlockCount; k++)
            {
                Stream.Write(BitConverter.GetBytes(fileConstants.SHA1Table[k]), 0, sizeof(int));
            }
            Stream.Write(BitConverter.GetBytes(fileConstants.valuedBytes), 0, BitConverter.GetBytes(fileConstants.valuedBytes).Length);
            Stream.Write(BitConverter.GetBytes(fileConstants.onDelete), 0, BitConverter.GetBytes(fileConstants.onDelete).Length);
            return true;

        }

        public bool Delete(string Name)
        {
            Encoding enc = Encoding.UTF8;
            FileStructConst localStruct = new FileStructConst();
            FileStructConst bufferedStruct = new FileStructConst();
            uint offset = Constatns.serviceStructSize;
            char[] c_name = new char[64 * sizeof(char)];
            char[] s_name = new char[64 * sizeof(char)];
            byte[] b_name = new byte[64 * sizeof(char)];
            for (int i = 0; i < (Encoding.UTF8).GetBytes(Name).Length; i++)
            {
                b_name[i] = (Encoding.UTF8).GetBytes(Name)[i];
            }
            int ind = (Encoding.UTF8).GetBytes(Name).Length;
            while (ind < 64 * sizeof(char))
            {
                b_name[ind] = 63;
                ind++;
            }
            c_name = enc.GetChars(b_name);
            for (int i = 0; i < Constatns.blockCount; i++)
            {
                if (Constatns.structSize[i] != 0)
                {
                    byte[] buffer = new byte[sizeof(uint)];
                    Stream.Position = offset + sizeof(uint);
                    Stream.Read(b_name, 0, b_name.Length);
                    s_name = enc.GetChars(b_name);
                    Stream.Position -= 4;
                    if (s_name.SequenceEqual(c_name))
                    {
                        localStruct = LoadFileStruct(offset);
                        break;

                    }
                    else
                    {
                        offset += Constatns.structSize[i];
                    }
                }

            }
            uint globalOffset = offset;
            offset = Constatns.serviceStructSize;
            Stream.Position = offset;
            for (int i = 0; i < Constatns.structSize.Length; i++)
            {
                if (Constatns.structSize[i] != 0)
                {
                    if (i == localStruct.Nubmer - 1)
                    {
                        Stream.Position += localStruct.structSize;
                    }
                    else
                    {
                        bufferedStruct = new FileStructConst();
                        bufferedStruct = LoadFileStruct(Stream.Position);
                        localStruct.SHA1Table.Distinct().Intersect(bufferedStruct.SHA1Table).ToList()
                            .ForEach(elem => localStruct.SHA1Table = localStruct.SHA1Table.Select(x => x == elem ? 0 : x).ToArray());
                        var buff = from p in localStruct.SHA1Table select p != 0;
                        if (buff.Count() == 0)
                        {
                            localStruct.usedBlocks = new uint[localStruct.deduplicatedBlockCount + localStruct.uniqueBlockCount];
                            break;
                        }

                    }
                }
                else
                {
                    break;
                }
            }

            uint freeBlocks = 0;
            //удаляются блоки
            //перед удалением, формируем индексы удаляемых блоков основываясь на хэше.
            int index = 0;
            localStruct.usedBlocks = localStruct.SHA1Table.Select(x =>
            {

                if (x == 0)
                {
                    index++;
                    return (uint)x;
                }
                else { return localStruct.usedBlocks[index++]; }

            }
                ).ToArray();
            for (int i = 0; i < localStruct.usedBlocks.Length; i++)
            {
                //непосредственное удаление блоков
                byte[] emptyBlock = new byte[Constatns.standartBlockSize];
                if (localStruct.usedBlocks[i] != 0)
                {
                    Stream.Position = Constatns.storageSize - localStruct.usedBlocks[i] * Constatns.standartBlockSize;
                    Stream.Write(emptyBlock, 0, emptyBlock.Length);
                    Constatns.indexFreeBlocks[localStruct.usedBlocks[i] - 1] = 0;
                    Constatns.fillBlocks--;
                    Constatns.freeBlocks++;
                }
            }
            //смещение структуры           
            int indexOnDelete = (int)localStruct.Nubmer - 1;
            var Summ = Constatns.structSize.Take(indexOnDelete + 1).Aggregate((av, e) => av + e);//+1
            uint localOffset = localStruct.structSize;
            offset = Constatns.serviceStructSize + Summ;
            try
            {
                while (Constatns.structSize[indexOnDelete + 1] != 0)
                {
                    bufferedStruct = new FileStructConst();
                    bufferedStruct = LoadFileStruct(offset);
                    bufferedStruct.Nubmer--;
                    WriteFileStruct(offset - localOffset, bufferedStruct);
                    offset += bufferedStruct.structSize;
                    Constatns.structSize[indexOnDelete] = bufferedStruct.structSize;
                    indexOnDelete++;
                }
                byte[] emptyStruct = new byte[localOffset];
                Stream.Position = offset - localOffset;
                Constatns.structSize[indexOnDelete] = 0;
                Stream.Write(emptyStruct, 0, emptyStruct.Length);
                ReFill();
                return true;
            }
            catch (IndexOutOfRangeException)
            {
                byte[] emptyStruct = new byte[localOffset];
                Stream.Position -= localOffset;
                Constatns.structSize[indexOnDelete] = 0;
                Stream.Write(emptyStruct, 0, emptyStruct.Length);
                ReFill();
                return true;
            }
        }

        public override byte[] Decrypt(byte[] Block)
        {
            byte[] buffer = new byte[64];
            byte[] resultBlock = new byte[Block.Length];
            string prePass = DecryptPass(Constatns.password);
            List<byte> btePass = (Encoding.UTF8).GetBytes(prePass).ToList();
            int ind = btePass.Count;
            if (ind == 0)
            {
                string newPass = Constatns.storageSize.ToString() + Constatns.blockCount.ToString();
                btePass = (Encoding.UTF8).GetBytes(newPass).ToList();
                if (btePass.Count >= 16)
                {
                    btePass.RemoveRange(16, btePass.Count - 16);
                }
            }
            else if (ind >= 16)
            {
                btePass.RemoveRange(16, btePass.Count - 16);
            }
            ind = btePass.Count;
            while (ind < 16)
            {
                btePass.Add((byte)ind);
                ind++;
            }
            byte[] Key = btePass.ToArray();
            byte[] IV = Key.Reverse().ToArray();
            Dictionary<int, byte[]> dict = new Dictionary<int, byte[]>();
            Dictionary<int, byte[]> result = new Dictionary<int, byte[]>();
            for (int i = 0; i < Block.Length / 64; i++)
            {
                dict.Add(i, Block.Skip(i * 64).Take(64).ToArray());
            }

            switch(Constatns.encryptionAlg)
            {
                case 1:
                    {
                        MemoryStream ms = new MemoryStream(Block);
                        foreach (var elem in dict)
                        {
                            buffer = new byte[64];
                            Aes aesAlg = Aes.Create();
                            aesAlg.Padding = PaddingMode.None;
                            ICryptoTransform encryptor = aesAlg.CreateDecryptor(Key, IV);
                            CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Read);
                            BinaryReader br = new BinaryReader(cs);
                            br.Read(buffer, 0, buffer.Length);
                            cs.FlushFinalBlock();
                            result.Add(elem.Key, buffer);
                        }
                        foreach (var elem in result)
                        {
                            elem.Value.CopyTo(resultBlock, elem.Key * 64);
                        }
                        break;
                    }

                case 2:
                    {
                        BlowFish bf = new BlowFish(Key);
                        bf.IV = IV.Skip(8).ToArray();
                        foreach (var elem in dict)
                        {
                            result.Add(elem.Key, bf.Decrypt_CBC(elem.Value));
                        }
                        foreach (var elem in result)
                        {
                            elem.Value.CopyTo(resultBlock, elem.Key * 64);
                        }
                        break;
                    }
                case 0:
                    {
                        resultBlock = Block;
                        break;
                    }
            }

            
            return resultBlock;
        }

        public byte[] EncryptPass(string Password)
        {
            byte[] aesKey = BitConverter.GetBytes(Constatns.storageSize).Concat(BitConverter.GetBytes(Constatns.standartBlockSize).
                ToArray()).Concat(BitConverter.GetBytes(Constatns.blockCount).ToArray()).ToArray();
            byte[] aesIV = aesKey.Reverse().ToArray();
            byte[] result = new byte[64 * sizeof(char)];
            string pass = Password;
            byte[] bc_name = new byte[64 * sizeof(char)];
            for (int i = 0; i < (Encoding.UTF8).GetBytes(Password).Length; i++)
            {
                bc_name[i] = (Encoding.UTF8).GetBytes(Password)[i];
            }
            int ind = (Encoding.UTF8).GetBytes(Password).Length;
            while (ind < 64 * sizeof(char))
            {
                bc_name[ind] = 63;
                ind++;
            }

            pass = (Encoding.UTF8).GetString(bc_name);
            /*TripleDESCryptoServiceProvider tripDES = new TripleDESCryptoServiceProvider();
            tripDES.Padding = PaddingMode.None;
            MemoryStream ms = new MemoryStream();
            ICryptoTransform encryptor = tripDES.CreateEncryptor(desKey, desIV);
            CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write);
            StreamWriter sw = new StreamWriter(cs);
            sw.Write(Password);
            cs.FlushFinalBlock();
            result = ms.ToArray();
            return result;*/
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = aesKey;
                aesAlg.IV = aesIV;
                aesAlg.Padding = PaddingMode.None;
                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key
, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt
, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(
csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(pass);
                        }
                        result = msEncrypt.ToArray();
                    }
                }
            }
            return result;
        }

        public string DecryptPass(byte[] Block)
        {
            byte[] aesKey = BitConverter.GetBytes(Constatns.storageSize).Concat(BitConverter.GetBytes(Constatns.standartBlockSize).
               ToArray()).Concat(BitConverter.GetBytes(Constatns.blockCount).ToArray()).ToArray();
            byte[] aesIV = aesKey.Reverse().ToArray();
            string result = "";
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = aesKey;
                aesAlg.IV = aesIV;
                aesAlg.Padding = PaddingMode.None;
                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(Block))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting 
                            // and place them in a string.
                            result = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }
            return result;

        }

        public override byte[] Encrypt(byte[] Block)
        {

            string prePass = DecryptPass(Constatns.password);
            byte[] resultBlock = new byte[Block.Length];
            List<byte> btePass = (Encoding.UTF8).GetBytes(prePass).ToList();
            int ind = btePass.Count;
            if (ind == 0)
            {
                string newPass = Constatns.storageSize.ToString() + Constatns.blockCount.ToString();
                btePass = (Encoding.UTF8).GetBytes(newPass).ToList();
                if (btePass.Count >= 16)
                {
                    btePass.RemoveRange(16, btePass.Count - 16);
                }
            }
            else if (ind >= 16)
            {
                btePass.RemoveRange(16, btePass.Count - 16);
            }
            ind = btePass.Count;
            while (ind < 16)
            {
                btePass.Add((byte)ind);
                ind++;
            }
            switch(Constatns.encryptionAlg)
            {
                case 1:
                    {
                        byte[] aesKey = btePass.ToArray();
                        byte[] aesIV = aesKey.Reverse().ToArray();                        
                        Dictionary<int, byte[]> dict = new Dictionary<int, byte[]>();
                        Dictionary<int, byte[]> result = new Dictionary<int, byte[]>();
                        for (int i = 0; i < Block.Length / 64; i++)
                        {
                            dict.Add(i, Block.Skip(i * 64).Take(64).ToArray());
                        }
                        foreach (var elem in dict)
                        {

                            Aes aesAlg = Aes.Create();
                            aesAlg.Padding = PaddingMode.None;
                            MemoryStream ms = new MemoryStream();
                            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesKey, aesIV);
                            CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write);
                            BinaryWriter bw = new BinaryWriter(cs);
                            bw.Write(elem.Value);
                            cs.FlushFinalBlock();
                            result.Add(elem.Key, ms.ToArray());
                        }
                        foreach (var elem in result)
                        {
                            elem.Value.CopyTo(resultBlock, elem.Key * 64);
                        }
                        break;
                    }
                case 2:
                    {
                        byte[] Key = btePass.ToArray();
                        byte[] IV = Key.Reverse().ToArray();
                        Dictionary<int, byte[]> dict = new Dictionary<int, byte[]>();
                        Dictionary<int, byte[]> result = new Dictionary<int, byte[]>();
                        for (int i = 0; i < Block.Length / 64; i++)
                        {
                            dict.Add(i, Block.Skip(i * 64).Take(64).ToArray());
                        }
                        BlowFish bf = new BlowFish(Key);
                        bf.IV = IV.Skip(8).ToArray();
                        foreach (var elem in dict)
                        {                           
                            result.Add(elem.Key, bf.Encrypt_CBC(elem.Value));
                        }
                        foreach (var elem in result)
                        {
                            elem.Value.CopyTo(resultBlock, elem.Key * 64);
                        }
                        break;
                    }
                case 0:
                    {
                        resultBlock = Block;
                        break;
                    }
            }
            return resultBlock;
        }
    }
}
