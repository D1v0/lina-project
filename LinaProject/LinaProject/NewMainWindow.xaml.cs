﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Reflection;
using LinaProject.Tier3GUI;
using LinaProject.Tier1_FileWork.LocalFile;
using LinaProject.Tier1_FileWork.Storage;
using BaseModels;
using System.Threading;

namespace LinaProject
{
    delegate void UpdateProgressBarDelegate(DependencyProperty dp, object value);
    delegate void UpdateLabelContent(DependencyProperty dp, object value);
    delegate void UpdateProgressMaximum(DependencyProperty dp, object value);
    /// <summary>
    /// Логика взаимодействия для NewMainWindow.xaml
    /// </summary>
    public partial class NewMainWindow : Window
    {
        BackgroundWorker worker, extractWorker;
        FileInfo[] files1, files2;
        FileWork FileToRW;
        StorageWork Storage;
        List<FilesDataInfoGrid> fileStructInfo = new List<FilesDataInfoGrid>();
        string Pass = "";
        NewWindowModel model;
        public NewMainWindow()
        {
            model = new NewWindowModel();
            InitializeComponent();
            this.DataContext = model;
            folderFiles.Items.Clear();
            worker = new BackgroundWorker();
            extractWorker = new BackgroundWorker();
            extractWorker.DoWork += new DoWorkEventHandler(extractWorker_DoWork);
            extractWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(extractWorker_Complete);

            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_Complete);
         //   MessageBox.Show("Произошла ошибка чтения/записи файла. Проверьте его доступность.","Ошибка!" , MessageBoxButton.OK,MessageBoxImage.Warning);
          // MessageBox.Show("Не удалось открыть хранилище.","Ошибка!" , MessageBoxButton.OK, MessageBoxImage.Error);
         //   MessageBox.Show("Возникла критическая ошибка. Приложение завершит работу!","Внимание!",  MessageBoxButton.OK, MessageBoxImage.Error);
          
        }

        void extractWorker_Complete(object sender, RunWorkerCompletedEventArgs e)
        {
          /*  UpdateProgressBarDelegate updType = new UpdateProgressBarDelegate(progress.SetValue);
            UpdateLabelContent updContent = new UpdateLabelContent(lbRecord.SetValue);
            Dispatcher.Invoke(updType, new object[] { System.Windows.Controls.ProgressBar.IsIndeterminateProperty, false });
            Dispatcher.Invoke(updContent, new object[] { System.Windows.Controls.Label.ContentProperty, "Завершено" });*/
            progress.IsIndeterminate = false;
            lb.Content = "Завершено";
            RefreshAll_PropertyChanged();
        }

        void extractWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BeginExtract();
        }

        void worker_Complete(object sender, RunWorkerCompletedEventArgs e)
        {
            Storage.ReFill();
            ReLoad();
            RefreshAll_PropertyChanged();
            lb.Content = "Завершено";

        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BeginWrite();   
        }

        public void BeginExtract()
        {
            if (Storage != null)
            {
                try
                {
                    UpdateLabelContent updContent = new UpdateLabelContent(lb.SetValue);
                    List<FilesDataInfoGrid> names = new List<FilesDataInfoGrid>();
                    foreach (FilesDataInfoGrid elem in model.beforeWrite)
                    {
                        names.Add(elem);
                    }
                    var result = MessageBox.Show("Будет извлечено по пути:\n" + model.PathToExtract, "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (result == MessageBoxResult.Yes)
                    {
                        foreach (var elem in names)
                        {
                            Dispatcher.Invoke(updContent, new object[] { System.Windows.Controls.Label.ContentProperty, elem.fileName });
                            Storage.Exctract(model.PathToExtract, elem.fileName);                           
                        }
                    }

                }
                catch { }
            }
            else
            {
                MessageBox.Show("Для начала откройте или создайте хранилище.", "Внимание!");
            }
        }

        public void BeginWrite()
        {

            if (Storage != null)
            {

                try
                {
                    UpdateProgressBarDelegate updProgress = new UpdateProgressBarDelegate(progress.SetValue);
                    UpdateLabelContent updContent = new UpdateLabelContent(lb.SetValue);
                    UpdateProgressMaximum updMax = new UpdateProgressMaximum(progress.SetValue);
                    List<View> names = new List<View>();
                    foreach (View elem in model.beforeWrite)
                    {
                        names.Add(elem);
                    }
                    foreach (var elem in names)
                    {
                        foreach (var exist in fileStructInfo)
                        {
                            if (elem.Name.Contains(exist.fileName))
                            {
                                MessageBox.Show("Файл с таким именем существует.", "Внимание!");
                                throw new Exception("Файл с таким именем существует.");
                            }
                        }
                        FileToRW = new FileWork(model.PathToRead + "\\" + elem.Name);
                        byte[] Block = new byte[Storage.Constatns.standartBlockSize];
                        uint blockCount = (uint)Math.Ceiling((double)FileToRW.Info.Length / Storage.Constatns.standartBlockSize);
                        model.blocCount = (int)blockCount;
                        // FileStructConst fileConstants = new FileStructConst();
                        model.localConst = Storage.CreateDepletedFileStruct(FileToRW.Info, blockCount);
                        if (model.localConst == null)
                        {
                            MessageBox.Show("Недостаточно свободного места.", "Внимание!");
                            break;
                        }
                        else
                        {
                            //   Tier3GUI.ProgressBar bar = new Tier3GUI.ProgressBar(1, (int)blockCount, 1);                            
                            byte[] buffer = new byte[Storage.Constatns.standartBlockSize];
                            Dispatcher.Invoke(updMax, new object[] {System.Windows.Controls.ProgressBar.MaximumProperty, (double)blockCount});
                            Dispatcher.Invoke(updContent, new object[] { System.Windows.Controls.Label.ContentProperty, elem.Name });
                            for (int i = 0; i < blockCount; i++)
                            {
                                Block = FileToRW.Read(Storage.Constatns.standartBlockSize);
                                model.localConst = Storage.WriteBlock(Block, blockCount, model.localConst);
                                Dispatcher.Invoke(updProgress, new object[] { System.Windows.Controls.ProgressBar.ValueProperty, (double)i });
                            }

                            Storage.WriteEnrichedFileStruct(model.localConst);
                            //  Storage.ReFill();
                        }
                    }
                    /* ReLoad();
                     RefreshAll_PropertyChanged();*/
                }
                catch { }
            }
            else
            {
                MessageBox.Show("Для начала откройте или создайте хранилище.", "Внимание!");
            }         
        }

        ObservableCollection<StorageView> storageFilesView
         {
             get
             {
                 var rand = new Random();
                 ObservableCollection<StorageView> res = new ObservableCollection<StorageView>();
                 foreach (var elem in files2)
                 {
                     
                     res.Add(new StorageView(elem.Name, elem.Length, Math.Round(rand.NextDouble(), 2)));
                 }
                 return res;
             }
         }

        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

         private void folderFiles_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
         {
             var displayName = GetPropertyDisplayName(e.PropertyDescriptor);

             if (!string.IsNullOrEmpty(displayName))
             {
                 e.Column.Header = displayName;
             }

         }

         public static string GetPropertyDisplayName(object descriptor)
         {
             var pd = descriptor as PropertyDescriptor;

             if (pd != null)
             {
                 // Check for DisplayName attribute and set the column header accordingly
                 var displayName = pd.Attributes[typeof(DisplayNameAttribute)] as DisplayNameAttribute;

                 if (displayName != null && displayName != DisplayNameAttribute.Default)
                 {
                     return displayName.DisplayName;
                 }

             }
             else
             {
                 var pi = descriptor as PropertyInfo;

                 if (pi != null)
                 {
                     // Check for DisplayName attribute and set the column header accordingly
                     Object[] attributes = pi.GetCustomAttributes(typeof(DisplayNameAttribute), true);
                     for (int i = 0; i < attributes.Length; ++i)
                     {
                         var displayName = attributes[i] as DisplayNameAttribute;
                         if (displayName != null && displayName != DisplayNameAttribute.Default)
                         {
                             return displayName.DisplayName;
                         }
                     }
                 }
             }

             return null;
         }

         private void storageFiles_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
         {
             var displayName = GetPropertyDisplayName(e.PropertyDescriptor);

             if (!string.IsNullOrEmpty(displayName))
             {
                 e.Column.Header = displayName;
             }
         }

         private void folderFiles_MouseDoubleClick(object sender, MouseButtonEventArgs e)
         {
             View selected = (View)folderFiles.SelectedItem;
             if (selected.Extention == "Папка с файлами")
             { model.path += "\\" + selected.Name; folderFiles.SelectedItem = 0; }
             if (selected.Name == "...")
             { model.path = System.IO.Path.GetDirectoryName(model.path); folderFiles.SelectedItem = 0; }
             if(selected.Extention == "Логический диск")
             { model.path = selected.Name; folderFiles.SelectedItem = 0; }
         }

         private void folderFiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
         {
             View selected = (View)folderFiles.SelectedItem;
             try
             {
                 
                 if (selected.Extention != ".lbsu")
                 {
                     OpenStorage.IsEnabled = false;
                     MenuOpenStorage.IsEnabled = false;
                 }
                 else
                 {
                     OpenStorage.IsEnabled = true;
                     MenuOpenStorage.IsEnabled = true;
                 }
                 try
                 {
                     if (selected.Extention == "Папка с файлами"||selected.Name==Storage.Constatns.storageName)
                     {
                         MenuAddFile.IsEnabled = false;
                         AddFile.IsEnabled = false;
                         Button source = (Button)this.AddFile;
                         ImageBrush content = source.Background as ImageBrush;
                         if (null != content)
                         {
                             content.ImageSource = new BitmapImage(new Uri("pack://application:,,,/LinaProject;component/Resources/CantAdd.ico", UriKind.RelativeOrAbsolute));
                         }

                     }
                     else
                     {
                         MenuAddFile.IsEnabled = true;
                         AddFile.IsEnabled = true;
                         Button source = (Button)this.AddFile;
                         ImageBrush content = source.Background as ImageBrush;
                         if (null != content)
                         {
                             content.ImageSource = new BitmapImage(new Uri("pack://application:,,,/LinaProject;component/Resources/add.ico", UriKind.RelativeOrAbsolute));
                         }

                     }
                     if (worker.IsBusy || extractWorker.IsBusy)
                     {
                         foreach(Button elem in FindVisualChildren<Button>(this) )
                         {
                             elem.IsEnabled = false;
                         }
                         foreach (MenuItem elem in FindVisualChildren<MenuItem>(this))
                         {
                             elem.IsEnabled = false;
                         }
                     }
                     else
                     {
                         foreach (Button elem in FindVisualChildren<Button>(this))
                         {
                             elem.IsEnabled = true;
                         }
                         foreach(MenuItem elem in FindVisualChildren<MenuItem>(this))
                         {
                             elem.IsEnabled = true;
                         }
                     }
                 }
                 catch
                 {
                     MenuAddFile.IsEnabled = false;
                     AddFile.IsEnabled = false;
                     Button source = (Button)this.AddFile;
                     ImageBrush content = source.Background as ImageBrush;
                     if (null != content)
                     {
                         content.ImageSource = new BitmapImage(new Uri("pack://application:,,,/LinaProject;component/Resources/CantAdd.ico", UriKind.RelativeOrAbsolute));
                     }
                 }
             }
             catch { };

         }

         private void storageFiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
         {
             try
             {
                 FilesDataInfoGrid selected = (FilesDataInfoGrid)storageFiles.SelectedItem;
                 if (Storage.Constatns != null)
                 {
                     if (selected.empty)
                     {
                         ExtractFile.IsEnabled = false; MenuExtractFile.IsEnabled = false;
                         DeleteFile.IsEnabled = false; MenuDeleteFile.IsEnabled = false;
                     }
                     else
                     {
                         ExtractFile.IsEnabled = true; MenuExtractFile.IsEnabled = true;
                         DeleteFile.IsEnabled = true; MenuDeleteFile.IsEnabled = true;
                     }
                 }
                 else
                 {
                     ExtractFile.IsEnabled = false; MenuExtractFile.IsEnabled = false;
                     DeleteFile.IsEnabled = false; MenuDeleteFile.IsEnabled = false;
                 }

             }
             catch
             {
                 ExtractFile.IsEnabled = false; MenuExtractFile.IsEnabled = false;
                 DeleteFile.IsEnabled = false; MenuDeleteFile.IsEnabled = false;
             }
         }

         void RefreshAll_PropertyChanged()
         {
             this.DataContext = model;
             try
             {
                 model.GetConstants(Storage.Constatns);
                 model.filesInfo = fileStructInfo;
                 model.PropChanged();
                 if(Storage.Constatns != null)
                 {
                     MenuClearData.IsEnabled = true;
                     MenuDeleteStorage.IsEnabled = true;
                     MenuCloseStorage.IsEnabled = true;
                     DeleteStorage.IsEnabled = true;
                     ClearStorage.IsEnabled = true;
                     CloseStorage.IsEnabled = true;
                 }
                 else
                 {
                     MenuClearData.IsEnabled = false;
                     MenuDeleteStorage.IsEnabled = false;
                     MenuCloseStorage.IsEnabled = false;
                     DeleteStorage.IsEnabled = false;
                     ClearStorage.IsEnabled = false;
                     CloseStorage.IsEnabled = false;
                 }
             }
             catch { }            
             //storageFiles.ItemsSource = model.dicstorage;
             // mainFilesInfo.ItemsSource = model.fileInfoData;
         }

         void ReLoad()
         {
             try
             {
                 fileStructInfo = new List<FilesDataInfoGrid>();
                 long offset = Storage.Constatns.serviceStructSize;
                 bool flag = true;
                 try
                 {
                     for (int i = 0; i < Storage.Constatns.blockCount; i++)
                     {
                         if (Storage.Constatns.structSize[i] != 0)
                         {
                             try
                             {
                                 FileStructConst fileConstants = new FileStructConst();
                                 fileConstants = Storage.LoadFileStruct(offset);
                                 offset += fileConstants.structSize;
                                 fileStructInfo.Add(new FilesDataInfoGrid(fileConstants, Storage.Constatns.standartBlockSize));
                             }
                             catch { }
                         }
                         else
                         { flag = false; break; }
                     }
                     RefreshAll_PropertyChanged();
                 }
                 catch { fileStructInfo.Add(new FilesDataInfoGrid()); RefreshAll_PropertyChanged(); }
             }
             catch { MessageBox.Show("Возникла критическая ошибка. Приложение завершит работу!", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Error); this.Close(); }
         }

         private void ConnectLocal_Click(object sender, RoutedEventArgs e)
         {


         }

         private void AddFile_Click(object sender, RoutedEventArgs e)
         {
             progress.IsIndeterminate = false;
             progress.Value = 0;
             progress.Minimum = 1;
             progress.Maximum = (double)model.blocCount;
             model.PathToRead = model.path;
             model.beforeWrite = folderFiles.SelectedItems;

             worker.RunWorkerAsync();

         }

         private void ExtractFile_Click(object sender, RoutedEventArgs e)
         {
             model.PathToExtract = model.path;
             model.beforeWrite = storageFiles.SelectedItems;
             lbRecord.Content = "Извлечение:";
             progress.IsIndeterminate = true;
             extractWorker.RunWorkerAsync();
         }

         private void DeleteFile_Click(object sender, RoutedEventArgs e)
         {

             if (Storage != null)
             {
                 try
                 {
                     List<FilesDataInfoGrid> names = new List<FilesDataInfoGrid>();
                     foreach (FilesDataInfoGrid elem in storageFiles.SelectedItems)
                     {
                         names.Add(elem);
                     }
                     foreach (var elem in names)
                     {
                         Storage.Delete(elem.fileName);
                     }
                 }
                 catch { }
             }
             else
             {
                 MessageBox.Show("Для начала откройте или создайте хранилище.", "Внимание!");
             }
             ReLoad();
             RefreshAll_PropertyChanged();
         }



         private void folderFiles_PreviewMouseDown(object sender, MouseButtonEventArgs e)
         {
             // mainFilesInfo.ItemsSource = model.onMouseTableClick(folderFiles.SelectedItem.ToString());
         }

         private void CreateStorage_Click(object sender, RoutedEventArgs e)
         {
             CreateStorage WindowCreateStorage = new CreateStorage(ref model);
             WindowCreateStorage.ShowDialog();
             if (model.create)
             {
                 Storage = new StorageWork();
                 Storage.CreateNewStorage(model.PathToStorage, model.storageSize, model.blockSize, model.password, model.encryptionAlg);
                 model.GetConstants(Storage.Constatns);
             }
             RefreshAll_PropertyChanged();
         }

         private void MenuOpenStorage_Click(object sender, RoutedEventArgs e)
         {
             if(Storage!=null)
             {
                 var result = MessageBox.Show("Текущее хранилище будет закрыто, продолжать?", "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Question);
                 if (result == MessageBoxResult.Yes)
                 {
                     Storage.Stream.Close();
                     Storage = null;
                     var pt = model.path;
                     model = new NewWindowModel();
                     model.path = pt;
                     var select = (View)folderFiles.SelectedItem;
                     model.PathToStorage = model.path + "\\" + select.Name;
                     Storage = new StorageWork();
                     Storage.Constatns = Storage.OpenWeak(model.PathToStorage);

                     LogIn login = new LogIn(ref model);
                     login.ShowDialog();
                     if (!Storage.Constatns.password.SequenceEqual(Storage.EncryptPass(model.password)))
                     {
                         MessageBox.Show("Неверный пароль", "Ошибка");
                         Storage.Stream.Close();
                         Storage = null;
                     }
                     else
                     {
                         Storage.Stream.Close();
                         Storage = new StorageWork();
                         model.GetConstants(Storage.Open(model.PathToStorage));
                         ReLoad();
                         model.storageName = model.Constants.storageName;

                         RefreshAll_PropertyChanged();
                     }
                     /* if(model.Constants==null)
                      {
                          MessageBox.Show("Неудалось открыть хранилище. Проверьте его доступность.", "Ошибка");
                      }*/
             
                 }
             }
             else
             {
                 var select = (View)folderFiles.SelectedItem;
                 model.PathToStorage = model.path + "\\" + select.Name;
                 Storage = new StorageWork();
                 Storage.Constatns = Storage.OpenWeak(model.PathToStorage);

                 LogIn login = new LogIn(ref model);
                 login.ShowDialog();
                 if (!Storage.Constatns.password.SequenceEqual(Storage.EncryptPass(model.password)))
                 {
                     MessageBox.Show("Неверный пароль", "Ошибка");
                     Storage.Stream.Close();
                     Storage = null;
                 }
                 else
                 {
                     Storage.Stream.Close();
                     Storage = new StorageWork();
                     model.GetConstants(Storage.Open(model.PathToStorage));
                     ReLoad();
                     model.storageName = model.Constants.storageName;

                     RefreshAll_PropertyChanged();
                 }
             }
           
         }

         private void MenuCloseStorage_Click(object sender, RoutedEventArgs e)
         {
             Storage.Stream.Close();
             Storage = null;
             model = new NewWindowModel();
             RefreshAll_PropertyChanged();
         }

         private void MenuClearData_Click(object sender, RoutedEventArgs e)
         {
             if (Storage != null)
             {
                 var result = MessageBox.Show("Очистка уничтожит все данные, продолжить?", "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Question);
                 if (result == MessageBoxResult.Yes)
                 {
                     try
                     {
                         StorageWork Buffer = Storage;
                         Buffer.Stream = Storage.Stream;
                         Storage.DeleteStorage(Storage.Stream);

                         Buffer.CreateNewStorage(Buffer.Path, Buffer.Constatns.storageSize, Buffer.Constatns.standartBlockSize, Buffer.DecryptPass(Buffer.Constatns.password), Buffer.Constatns.encryptionAlg);
                         Buffer.Stream.Close();
                         string path = Buffer.Path;
                         Buffer = null;
                         Storage.Open(path);
                         model.GetConstants(Storage.Constatns);
                         model.storageName = Storage.Constatns.storageName;
                     }
                     catch { MessageBox.Show("Возникла критическая ошибка. Приложение завершит работу!", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Error); this.Close(); }
                     
                     
                 }
                 ReLoad();
                 RefreshAll_PropertyChanged();
             }
             else
             {
                 MessageBox.Show("Для начала откройте или создайте хранилище.", "Внимание!");
             }
         }

         private void MenuDeleteStorage_Click(object sender, RoutedEventArgs e)
         {
             if (Storage != null)
             {
                 var result = MessageBox.Show("Вы уверенны что хотите удалить хранилище?", "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Question);
                 if (result == MessageBoxResult.Yes)
                 {
                     Storage.DeleteStorage(Storage.Stream);
                     model = new NewWindowModel();
                 }
                 RefreshAll_PropertyChanged();
             }
             else
             {
                 MessageBox.Show("Для начала откройте или создайте хранилище.", "Внимание!");
             }
         }

         private void selectExtractPath_Click(object sender, RoutedEventArgs e)
         {
             System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
             dlg.ShowDialog();
             if (!String.IsNullOrEmpty(dlg.SelectedPath))
             {
                 model.PathToExtract = dlg.SelectedPath;
             }
         }


    }

   

    class StorageView
    {
        [DisplayName("Имя файла")]
        public string Name { get; set; }
        [DisplayName("Размер (блок)")]
        public long Size { get; set; }
        [DisplayName("Процент дупликации")]
        public double Extention { get; set; }
        public StorageView(string Name , long Size, double Extention)
        {
            this.Name = Name;
            this.Size = Size/4096;
            this.Extention = Extention;
            
        }
    }
}
